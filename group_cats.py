import itertools
import json
from collections import namedtuple


Sentence = namedtuple("Sentence", "text battery trial")
class Sentence(Sentence):
    @property
    def signature(self):
        return self.words[:2] + self.words[-1:]
    @property
    def words(self):
        return self.text.split()
    def __str__(self):
        return "{0} (battery {1} trial {2})".format(self.text, self.battery + 1,
                                                    self.trial + 1)

if __name__ == "__main__":
    with open("files.json") as fh:
        data = json.load(fh)
        flat = [Sentence(text, bat_num, sent_num)
                for (bat_num, battery) in enumerate(data)
                for (sent_num, text) in enumerate(battery)]
        sort_fun = lambda x: x.signature
        sorted_data = sorted(flat, key=sort_fun)
        for key, items in itertools.groupby(sorted_data, sort_fun):
            print(" ".join(key))
            for item in items:
                print("\t", item)
            print()
