The experiment is:

- Participant hears a prompt.
- Participant repeats prompt into tape recorder (which is rolling the whole
  time).
- Participant presses button to indicate they're ready for the next prompt.

That's it.

Share the battery creation process with other folks, like Francis.
