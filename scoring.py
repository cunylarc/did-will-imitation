"""Different scoring functions for judging DWI participants' sequence
imitations

All these functions return an integer score. 0 would be a perfect response
according to the given function, and the higher the number the worse they did.

"""

def types_inclusive(stimulus, response):
    """A zero score indicates the participant did not exclude any of the presented
    stimuli, regardless of order or absolute number of responses.

    A point is given for each type appearing in the response that does not
    appear in the input.

    >>> types_inclusive([1,2,3], [3,2,1])
    0
    >>> types_inclusive([1,2,3], [3,2,1,2,3,3,3])
    0
    >>> types_inclusive([1,2,3], [1])
    2
    >>> types_inclusive([1,2,3], [5,1,2,8])
    1

    """
    pass

def tokens_exclusive(stimulus, response):
    """A zero score indicates the participant did not produce and extraneous types
    that did not appear in the input.

    A point is given for each type (TODO: or should it be token?) given in the
    response that did not appear in the stimulus

    >>> tokens_exclusive([1,2,3], [1,2,3])
    0
    >>> tokens_exclusive([1,2,3], [])
    0
    >>> tokens_exclusive([1,2,3], [4,2])
    1
    >>> tokens_exclusive([1,2,3], [5,5,6,7])
    3

    """
    pass

def absolute_ordering(stimulus, response):
    """A zero score indicates a perfect response.

    A point is given for every index in the response that does not match the
    same index in the stimulus

    >>> absolute_ordering([1,2,3], [1,2,3])
    0
    >>> absolute_ordering([1,2,3], [1,1,2,3])
    3
    >>> absolute_ordering([1,2,3], [2,1,2,3])
    4
    >>> absolute_ordering([1,2,3], [1,2,3,2])
    1

    """

def ordered_pairs(stimulus, response):
    """A zero score indicates that every ordered pair appearing in the response
    appeared in the stimulus.

    A point is given for each pair missing from the response and for each pair
    present in the response not appearin in the stimulus.

    TODO: sentinal nodes?

    >>> ordered_pairs([1,2,3], [1,2,3])
    0
    >>> ordered_pairs([1,2,3,1], [3,1,2])
    2
    TODO: continue
    """

def ordered_pairs(stimulus, response):
    """Returns a 3-tuple of (hits, misses, insertions)

    >>> ordered_pairs([1,2,9,5], [1,2,3,6,9])
    (2, 3, 4)
    >>> ordered_pairs([6,7,4,2], [6,4,2,7])
    (2, 3, 3)
    >>> ordered_pairs([2,7,6,3], [2,6,7,3])
    (2, 3, 3)
    >>> ordered_pairs([9,1,2], [1,2,9])
    (1, 2, 3)
    >>> ordered_pairs([5,8,4], [5,8,4])
    (4, 0, 0)
    >>> ordered_pairs([9,1,2], [2,3,5])
    (0, 4, 4)
    >>> ordered_pairs([9,1,2], [2,3,5])
    (0, 4, 4)

    """
    pass

def unordered_pairs(stimulus, response):
    pass

def edit_distance(stimulus, response):
    pass
