import json
from os.path import exists, join, dirname

data = json.load(open("files.json"))

for battery in data:
    for name in battery:
        audio_path = "/home/paul/ardour/did-will-imitation/export/sentences/"
        path = join(audio_path, name) + ".wav"
        if not exists(path):
            print path
